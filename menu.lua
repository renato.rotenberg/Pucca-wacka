local Gamestate = require "lib.gamestate"
local suit = require "lib.SUIT"
local game -- = require "game"

local menu = {}
local timeSlider = {value = 30, min = 0, max = 60}
local warpSlider = {value = 1, min = 0, max = 5}

function menu:init()
  -- Avoid circular requires
  game = require "game"
end

function menu:update()
  suit.layout:reset(250, 200, 10, 10)

  if suit.Button("Play!", suit.layout:row(250, 30)).hit then
    Gamestate.switch(game, timeSlider.value, warpSlider.value)
  end

  suit.Label("Game time", suit.layout:row())
  suit.layout:push(suit.layout:row())
  suit.layout:padding(3)
  suit.Slider(timeSlider, suit.layout:col(160, 20))
  suit.Label(("%.02f"):format(timeSlider.value), suit.layout:col(40))
  suit.layout:pop()

  suit.Label("Warp time", suit.layout:row())
  suit.layout:push(suit.layout:row())
  suit.layout:padding(3)
  suit.Slider(warpSlider, suit.layout:col(160, 20))
  suit.Label(("%.02f"):format(warpSlider.value), suit.layout:col(40))
  suit.layout:pop()
end

return menu

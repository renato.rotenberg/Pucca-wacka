local Gamestate = require "lib.gamestate"
local menu = require "menu"
local suit = require "lib.SUIT"

function love.load()
  if love.system.getOS == "Android" then
    love.window.setMode(0,0)
  end
  Gamestate.switch(menu)
end

function love.update(dt)
  Gamestate.update(dt)
end

function love.draw()
  Gamestate.draw()
  suit.draw()
end

function love.mousepressed(x, y, button, isTouched)
  Gamestate.mousepressed(x, y, button, isTouched)
end

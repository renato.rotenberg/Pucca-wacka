local Gamestate = require "lib.gamestate"
local Pucca = require "pucca"
local gameover = require "gameover"

local lg = love.graphics

local puccaGlobal

local game = {}

function game:enter(previous, gameTime, warpTime)
  local x, y = love.graphics.getDimensions()
  puccaGlobal = Pucca(x, y, x*0.00015, warpTime)
  self.time = gameTime
  self.score = 0
end

function game:update(dt)
  puccaGlobal:update(dt)
  self.time = self.time - dt
  if self.time <= 0 then
    Gamestate.switch(gameover, self.score)
  end
end

function game:draw()
  puccaGlobal:draw()
  lg.print("time: " .. math.ceil(self.time), 0, 0)
  lg.print("score: " .. self.score, 0, 15)
end

function game:mousepressed(x, y, button, isTouched)
  if puccaGlobal:isHit(x, y) and puccaGlobal.state ~= "hit" then
    puccaGlobal:hit()
    self.score = self.score + 100
  end
end

return game

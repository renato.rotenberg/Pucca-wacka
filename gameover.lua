local Gamestate = require "lib.gamestate"
local suit = require "lib.SUIT"
local menu = require "menu"

local lg = love.graphics

local gameover = {}

function gameover:enter(previous, score)
  self.score = score or 0
end

function gameover:update()
  if suit.Button("Back to menu", 300, 360, 250, 30).hit then
    Gamestate.switch(menu)
  end
end

function gameover:draw()
  lg.print("Gameover! your score was " .. self.score, 300, 400)
end

return gameover

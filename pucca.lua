local Class = require "lib.class"

local lg = love.graphics
local lm = love.math

local img = {
  pucca = lg.newImage("assets/puccaseal2.png"),
  puccaXmas = lg.newImage("assets/puccaseal2xmas.png"),
  puccaHit = lg.newImage("assets/puccaseal2hit.png")
}

--------------------------------------------------------------------------------

local Pucca = Class{}

function Pucca:init(screenX, screenY, scale, warpTime)
  self.img = img.pucca
  self.imgHit = img.puccaHit
  self.screenX = screenX
  self.screenY = screenY
  self.scale = scale or 0.1
  self.warpTime = warpTime or 1
  self.hitTime = self.warpTime/2
  self.w = img.pucca:getWidth()*self.scale
  self.h = img.pucca:getHeight()*self.scale

  self:warp()
  self.state = "not hit"
  self.time = self.warpTime
end

function Pucca:update(dt)
  if self.state == "not hit" then
    self.time = self.time - dt
    if self.time <= 0 then
      self:warp()
      self.time = self.time + self.warpTime
    end
  elseif self.state == "hit" then
    self.time = self.time - dt
    if self.time <= 0 then
      self.state = "not hit"
      self.time = self.warpTime
      self:warp()
    end
  end
end

function Pucca:draw()
  if self.state == "not hit" then
    lg.draw(self.img, self.x, self.y, 0, self.scale)
  elseif self.state == "hit" then
    lg.draw(self.imgHit, self.x, self.y, 0, self.scale)
  end
end

function Pucca:isHit(x, y)
  return self.x <= x and x <= self.x + self.w
         and
         self.y <= y and y <= self.y + self.h
end

function Pucca:hit()
  self.state = "hit"
  self.time = self.hitTime
end

function Pucca:warp()
  self.x = lm.random(0, self.screenX - self.w)
  self.y = lm.random(0, self.screenY - self.h)
end

return Pucca
